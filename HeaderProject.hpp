#pragma once
#ifndef PROJECT_HEADERPROJECT_HPP
#define PROJECT_HEADERPROJECT_HPP

#endif //PROJECT_HEADERPROJECT_HPP

#include <iostream>
using namespace std;

namespace fun
{

    const int N = 100;

    void InputMatrix(int str, int stolb, int matrix[N][N]);


    int OdinakStolbi(int Odinprov, int n , int m, int mas[N][N], int OdinChisl);


    void OutputMatrix(int str, int stolb, int matrix[N][N]);


    int PoiskEasy(int n ,int m , int mas[N][N], int arr[N][N] , int SumOfSimple );


    int SimpleNum(int a);


    void MasvArr(int a , int b,int arr[N][N],int mas[N][N]);


    void SumStroki(int sum,int a, int b, int Sumof_Strok[N],int matrix[N][N]);


    void SortStr(int n, int m, int Sumof_Strok[N], int mas[N][N]);


}