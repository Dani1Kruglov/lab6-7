#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include "HeaderProject.hpp"// главная штука
using namespace std;
/*    6 работа
 *
 *
 * если есть в матрице два одинаковых столбца и хотя бы один элемент - модуль которого простое число =>
 * сортировать строки матрицы по возрастанию суммы модулей элементов
 *
 * По действиям:
 * 1) цикл сравнения столбцов на одинаковые
 * 2) цикл поиска модуля простого числа в матрице
 * 3) цикл сортировки строк по неубыванию суммы модулей элементов
 *
 * Примерные значеня - матрица 3 на 4
 *
1 10 15 10
4 8 5 8
0 9 9 9

Вывод:

 4 8 5 8
0 9 9 9
1 10 15 10
 */
int main()
{
    const int N = 100;
    cout<<"Введите размер матрицы:"<<endl;
    int n,m;
    cin>>n;//количество строк i
    cout<<"на"<<endl;
    cin>>m;//количество столбцов j
    int mas[N][N];
    int arr[N][N];
    int SumOfSimple = 0;
    int OdinChisl = 0;
    int Odinprov = 0;
    int Sumof_Strok[n];
    int Sum = 0;
    cout<<"Введите матрицу: "<<endl;

    fun::InputMatrix(n, m, mas);

    fun::MasvArr(n,m, arr, mas);


    if((fun::PoiskEasy( n , m , mas, arr, SumOfSimple ) > 0) && ( fun::OdinakStolbi(Odinprov ,  n ,  m,  mas,  OdinChisl) != 0) )
    {

        fun::SumStroki(Sum, n, m, Sumof_Strok, mas);

        fun::SortStr( n, m, Sumof_Strok, mas);

        fun::OutputMatrix(n, m, mas);

    }   //сортировка строк массив по возрастанию их суммы и их вывод

    else
        cout<<"Нет простого числа или нет равных по значениям элементов столбцов"<<endl;

    return 0;
}





