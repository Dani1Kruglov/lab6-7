
#include <iostream>
#include "HeaderProject.hpp"

using namespace std;

namespace fun {

    void InputMatrix(int str, int stolb, int matrix[N][N])
    {
        int x;
        for (int i = 0; i < str; i++)
            for (int j = 0; j < stolb; j++)
            {
                std::cin >>  matrix[i][j];
            }
    }

    int OdinakStolbi(int Odinprov, int n , int m, int mas[N][N], int OdinChisl)
    {
        for (int j1 = 0; j1 < m - 1; j1++) // цикл поиска одинаковых столбцов
        {
            for (int i = 0; i < n; i++)
            {
                for (int j2 = j1 + 1; j2 < m; j2++)
                {
                    if (mas[i][j1] == mas[i][j2])
                    {
                        if (i == 0)
                        {
                            OdinChisl++;
                            for (int i2 = 1; i2 < n; i2++)
                            {
                                if (mas[i2][j1] == mas[i2][j2])
                                {
                                    OdinChisl++;
                                }
                                else
                                {
                                    OdinChisl = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (OdinChisl == n)// если равно количеству строк , то одинаковые
            {
                Odinprov++;
                break;
            }
            OdinChisl = 0;
        }
        return OdinChisl;
    }

    void OutputMatrix(int str, int stolb, int matrix[N][N])
    {
        for (int i = 0; i < str; i++)
        {
            for (int j = 0; j < stolb; j++)
                std::cout << matrix[i][j] << " ";
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    int PoiskEasy( int n ,int m , int mas[N][N], int arr[N][N] , int SumOfSimple )
    {
        for(int  i = 0; i < n; i++)// поиск простых чисел в матрице с помощью функции SimpleNum
        {
            for(int  j = 0; j < m; j++)
            {
                if(mas[i][j] < 0)
                {
                    arr[i][j]=mas[i][j]*(-1);
                    if(fun::SimpleNum(arr[i][j]))
                        SumOfSimple++;

                }
                else if(mas[i][j]==0 || mas[i][j]==1)
                    continue;
                else
                {
                    if(fun::SimpleNum(arr[i][j]))
                        SumOfSimple++;

                }
            }
        }
        return SumOfSimple;
    }

    int SimpleNum(int a)
    {
        for(int i=2;i <= a/2;i++)
            if( (a % i) == 0 )
                return 0;
        return 1;
    }

    void MasvArr(int a , int b,int arr[N][N],int mas[N][N])
    {
        for(int  i = 0; i < a; i++)//значения массива mas  в массив arr
            for(int  j = 0; j < b; j++)
                arr[i][j]=mas[i][j];
    }

    void SumStroki(int sum,int a, int b, int Sumof_Strok[N],int matrix[N][N])
    {
        for(int i = 0; i < a ;i++) //цикл для подсчета сумм ячеек каждой строки
        {
            for(int j =0 ; j < b ;j++)
            {
                sum += matrix[i][j];
                if(j == b-1)
                {
                    Sumof_Strok[i] = sum;
                    sum = 0;
                }
            }
        }
    }

    void SortStr(int n, int m, int Sumof_Strok[N], int mas[N][N])
    {
        for(int i =0; i < n-1; i++) // цикл для сортировки строк матрицы по неубыванию
        {
            for (int j = i + 1; j < n; j++)
            {
                int temp[n][m];
                if(Sumof_Strok[i] > Sumof_Strok[j])
                {
                    for(int k = 0; k < m; k++)
                    {
                        temp[i][k]=mas[i][k];
                        mas[i][k]=mas[j][k];
                        mas[j][k]=temp[i][k];
                    }
                    swap(Sumof_Strok[i],Sumof_Strok[j]);
                }
            }
        }
    }
}